from django import forms
from . import models

class StatusModelForm(forms.ModelForm):
    class Meta:
        model = models.StatusModel
        fields = [
            'status'
        ]
        widgets = {
            'status': forms.Textarea(attrs={
                'row': 5,
                'cols': 60,
                'style': 'resize:none'
            })
        }