from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import StatusModel
from .forms import StatusModelForm


# Create your views here.
def landing_page(request):
    if request.method == 'POST':
        form = StatusModelForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('status:landing')
    form = StatusModelForm()
    statuses = StatusModel.objects.all()
    response = {
        'form': form,
        'statuses': statuses
    }
    return render(request, 'base.html', response)