from django.test import TestCase, Client
from django.urls import reverse, resolve
from datetime import datetime, timedelta
from .views import landing_page
from .models import StatusModel
from .forms import StatusModelForm

# Create your tests here.
class StatusTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.data_form = {
            'status': 'Coba Coba'
        }

    def test_ada_url_landing_page(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_tidak_ada_url_landing_page_slash_baru(self):
        response = self.client.get('/baru')
        self.assertEqual(response.status_code, 404)

    def test_fungsi_dipanggil_jika_url_landing_page_dinyalakan(self):
        response = resolve(reverse('status:landing'))
        self.assertEqual(response.func, landing_page)

    def test_bisa_membuat_models(self):
        StatusModel.objects.create(status='Coba Coba')
        self.assertEqual(StatusModel.objects.all().count(), 1)
        self.assertEqual(
            StatusModel.objects.get(id=1).status, 'Coba Coba'
        )

    def test_bisa_membuat_form(self):
        form = StatusModelForm(data=self.data_form)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(StatusModel.objects.all().count(), 1)
        self.assertEqual(
            StatusModel.objects.get(id=1).status, 'Coba Coba'
        )

    def test_waktu_status_dibuat_sama_dengan_waktu_sekarang(self):
        StatusModel.objects.create(
            status='Coba Coba'
        )
        statusNow = (StatusModel.objects.get(status='Coba Coba').waktu + timedelta(hours=7)).strftime("%H:%M")
        now = datetime.now().strftime("%H:%M")
        self.assertEqual(
            statusNow, now
        )
    
    def test_tanggal_status_dibuat_sama_dengan_tanggal_sekarang(self):
        StatusModel.objects.create(
            status = 'Coba Coba'
        )
        statusNow = StatusModel.objects.get(status='Coba Coba').waktu.strftime("%d/%m")
        now = datetime.now().strftime("%d/%m")
        self.assertEqual(
            statusNow, now
        )

    def test_fungsi_landing_views_GET(self):
        response = self.client.get(reverse('status:landing'))
        content = response.content.decode('utf8')
        self.assertIn('<h1', content)
        self.assertIn('My Status', content)

    def test_buka_landing_page_method_GET(self):
        response = self.client.get(reverse('status:landing'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'base.html')

    def test_tambah_status_pada_halaman(self):
        statusMessage = 'Coba Coba'
        response = self.client.post(reverse('status:landing'), {
            'status': statusMessage
        })
        content = self.client.get(reverse('status:landing')).content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(StatusModel.objects.all().count(), 1)
        self.assertIn(
            statusMessage, content
        )

    def test_models_punya_str(self):
        status = StatusModel.objects.create(
            status= 'Coba Coba'
        )
        self.assertEqual(str(status), 'Coba Coba')