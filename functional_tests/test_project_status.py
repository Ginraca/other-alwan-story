from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from status.models import StatusModel  
from status.forms import StatusModelForm
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase
from django.urls import reverse
import time

class TestProjectMyStatus(StaticLiveServerTestCase):
    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.close()

    def test_title_ada(self):
        # User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url)

        # time.sleep(10)
        # User melihat title website yang dibuka tertulis My Status
        self.assertIn('My Status', self.browser.title)

    def test_status_berhasil_ditambahkan_di_page(self):
        #User membuka browser dengan mengakses url yang mereka ketahui
        self.browser.get(self.live_server_url)

        #User melihat ada textfield yang dapat diisi, dan ia mengisi textfield tersebut
        #dengan memasukkan kalimat Coba Coba
        element = self.browser.find_element_by_tag_name("form").find_element_by_tag_name('textarea')
        testInput = 'Coba Coba'
        element.send_keys(testInput)
        #User mengklik tombol submit di sebelah textfield
        elementTombol = self.browser.find_element_by_tag_name('form').find_element_by_id('submit-status')
        elementTombol.click()       

        #User dapat melihat hasil status yang ia input di textfield di halaman web
        self.assertIn(
            testInput,
            self.browser.page_source
        )
